#!/usr/bin/env python3
import os, sys, json, argparse, subprocess, hashlib, pathlib, base64, logging, psutil
from pathlib import Path
from os import walk
from multiprocessing import Pool
from shutil import rmtree
from config import *

if path_root == '':
    path_root = os.environ.get('HOME', '/var/tmp')

list_processed_path = path_root + '/list_checked'

arg_parser = argparse.ArgumentParser(description='Generate video contact sheet.')
arg_parser.add_argument('-d', help='Debug output', action='store_true')
arg_parser.add_argument('-i', help='Request inital run', action='store_true')
args = arg_parser.parse_args()

putil_pro = psutil.Process()
putil_pro.ionice(psutil.IOPRIO_CLASS_IDLE)
putil_pro.nice(19)

if args.d == True:
    logging.basicConfig(level=logging.DEBUG)
    logging.info('Debug output ENABLE')
elif args.d == False:
    logging.basicConfig(level=logging.INFO)
    logging.info('Debug output disable')
else:
    raise Exception('args not working', args, args.d)

if args.i:
    initial_run_requested = True
    logging.info('Inital run required.')
else:
    initial_run_requested = False

def jpg_filename(path):
    path_str = str(path)
    hash = hashlib.md5()
    hash.update(path_str.encode('utf-8','backslashreplace'))
    output_path=pathlib.Path(output_prefix + '/' + hash.hexdigest() + '.jpg')
    
    return output_path
    
def create_thumbnail(path):
    output_path = jpg_filename(path)
    output_path_str = str(output_path)
    cmd = ["bash", vcs_path, '-U0', '-j', '-c', '4', '-H', '240', '-i', '600s', path, '-o', output_path_str]

    if output_path.is_file():
        logging.debug(f"Skipping, as the following output jpg exists: {output_path}")
        return True

    else:
        logging.debug(f"Creating thumbnail for: {path}")
        run_vcs = subprocess.run(cmd, capture_output=True, check=True, shell=False)
#         run_vcs.wait()
        return True

def create_html():
    logging.info('Creating HTML gallery.')
    thumbs_path_str = output_prefix + '/__thumbs'
    logging.debug(f"Removing {thumbs_path_str}")
    try:
        rmtree(thumbs_path_str)
    except FileNotFoundError:
        logging.debug(f"{thumbs_path_str} not found")
    
    return subprocess.run(["bash", gallery_sh_path], cwd=output_prefix, capture_output=True, check=True, shell=False)

file_list = set() # The set of files in path_search
for path_search_item in path_search:
    for (dirpath, dirnames, filenames) in os.walk(path_search_item):
        for file in filenames:
            path_object = pathlib.Path(dirpath) / file
            file_size = path_object.stat().st_size/1024/1024 #in MB
            if path_object.suffix in extension_to_search and file_size >= file_size_threshold:
                file_list.add( str(path_object.resolve()) )
                #TBD: exclude certain dir?

extra_list = set()

try: # Test if the processed list existed
    list_processed = open(list_processed_path, "r")
except FileNotFoundError as e:
    logging.info('Processed list not found.')
#     logging.debug('Processed list not found', e)
#     open(list_processed_path, 'x')
    initial_run = True
else:
    logging.info('Processed list found.')
    file_processed_list = set(json.load(list_processed))
    initial_run = False
#     logging.debug('file_processed_list', file_processed_list)

if initial_run or initial_run_requested:
    logging.info('Creating new gallery.')
    pending_list = file_list        

else:
    logging.info('Updating gallery.')
    pending_list = set(file_list) - set(file_processed_list)
    extra_list = file_processed_list - file_list
    
with Pool(processes=multi_process_count) as pool:
    pool.map(create_thumbnail, pending_list)

if len(extra_list) > 0:
    logging.info(f"Removing extra {len(extra_list)} file(s)")
    logging.debug(f"len(extra_list) = {len(extra_list)}")
    for extra_file in extra_list:
        extra_file_path = pathlib.Path(extra_file)
        logging.debug(f"Extra file found: {extra_file}")
        extra_file_jpg_path = jpg_filename(extra_file_path)
        
        logging.debug(f"Removing: {str(extra_file_jpg_path)}")
        pathlib.Path(extra_file_jpg_path).unlink()

if len(pending_list) > 0 or len(extra_list) > 0:
    create_html()
else:
    logging.info('No update needed.')

with open(list_processed_path, "w") as file:
    file_list_out = list(file_list)
    json.dump(file_list_out, file)