### Required configuration: ###

# The directory(s) containing videos to be scanned
# Absoulute path
path_search = {"CHANGE_ME"}

# Only file with the following suffix will be scanned
# Default: {'.mp4', '.mkv', '.avi'}
extension_to_search = {'.mp4', '.mkv', '.avi'}

# Also, only file with size larger than this value will be scanned
# Value in MiB, i.e. 1 = 1 MiB = 1024 * 1024 bytes
# Default: 1000 (1 GiB)
file_size_threshold = 1000

# Absoulute path of saving thumbnails
output_prefix = 'path_to_store_thumbnails'

# Absoulute path of vcs (Video Contact Sheet *NIX)
# Can be my fork (https://gitlab.com/end_my_suffering/vcs/-/blob/master/vcs)
# Or the original one (http://p.outlyer.net/vcs)
vcs_path = 'path_to_vcs'

# Absoulute path of gallery.sh
# Can be my fork (https://gitlab.com/end_my_suffering/gallery_sh/-/blob/master/gallery.sh)
# Or the original one (https://github.com/Cyclenerd/gallery_shell/-/blob/master/gallery.sh)
gallery_sh_path = 'path_to_gallery.sh'

# Defines how many parallel processes
# when generating video contact sheets.
# >1 yields better performace for multi-core CPU.
# Recommend: CPU core count / 2
# Required: > 0
multi_process_count = 2

# Defines the root of saving data
# Leave blank to use HOME directory (default)
# Default: ''
path_root = ''